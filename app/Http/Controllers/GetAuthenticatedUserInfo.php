<?php

namespace App\Http\Controllers;

use App\UserAccess;
use App\UserDetails;
use Illuminate\Http\Request;

class GetAuthenticatedUserInfo extends Controller
{
    /**
     * Get the authenticated user's information
     *
     * @return Response
     */
    public function __invoke()
    {
        $user = request()->user();

        $user->load([
            'accessControls',
            'accessControls.module' => function ($query) {
                $query->orderBy('created_at');
            },
            'userDetails',
        ]);

        return response($user);
    }
}
