<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\User;

class UserDetails extends Model
{

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'first_name',
        'middle_name',
        'last_name',
        'birth_date',
        'phone_number',
        'mobile_number',
        'city',
        'address',
        'zip_code',
        'image',
        'user_id',
    ];

    /**
     * Get the user that owns the shipper.
     *
     * @return BelongsTo
     */
    public function user()
    {
        return $this->belongsTo(User::class);
    }
}
