<?php

//String Values per Module
if (!function_exists('getStringHelperLogin')) {
    function getStringHelperLogin($index)
    {
        $stringArray = [
            'LoginInvalidRequest' => 'Invalid Request. Please enter a username or a password.',
            'LoginInvalidCredentials' => 'Invalid Credentials',
        ];

        return $stringArray[$index];
    }
}

if (!function_exists('getStringHelperError')) {
    function getStringHelperError($index)
    {
        $stringArray = [
            '500' => 'Something went wrong on the server',
        ];

        return $stringArray[$index];
    }
}
