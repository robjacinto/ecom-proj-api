<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Laravel\Passport\HasApiTokens;
use App\UserDetails;

class User extends Authenticatable
{
    use Notifiable, HasApiTokens;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'email',
        'password',
        'username',
        'is_web',
        'is_cms',
        'status',
        'login_attempts',
        'account_type',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    public function findForPassport($email)
    {
        return self::where('email', $email)->first();
    }

    /**
     * Increment the login attempts of the user.
     *
     * @return void
     */
    public function incrementLoginAttempts()
    {
        $this->increment('login_attempts');

        if ($this->login_attempts >= 3) {
            $this->deactivate();
        }
    }

    /**
     * Clear the user's number of login attempts.
     *
     * @return void
     */
    public function clearLoginAttempts()
    {
        $this->login_attempts = 0;
        $this->save();
    }

    /**
     * Deactivate the user.
     *
     * @return void
     */
    public function deactivate()
    {
        $this->status = 0;

        $this->save();
    }

    /**
     * The shipper associated with the web profile.
     *
     * @return HasOne
     */
    public function userDetails()
    {
        return $this->hasOne(UserDetails::class);
    }

    public function accessControls()
    {
        return $this->hasMany(AccessControl::class);
    }
}
